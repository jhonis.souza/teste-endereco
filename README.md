A aplicação foi construída pensando na arquitetura de microserviços e por esse motivo, foi construída
separadamente da consulta de CEP's, para permitir uma maior flexibilidade, desempenho e gerenciamento
de cada uma independendo da outra aplicação.

A persistência do endereço está sendo feita via controller para permitir a validação dos campos e
principalmente a validação do CEP. O acesso aos métodos do CRUD pode ser feito conforme lista abaixo.

POST: _http://servidor:porta/endereco/save_

    descrição: inserir ou atualizar um endereço (caso o id seja informado, o endereço será atualizado)
    body: objeto JSON com o conteúdo do endereço
    retorno: objeto JSON com o conteúdo do endereço cadastrado

GET: _http://servidor:porta/endereco

    descrição: listar todos os endereços cadastrados (seguindo o formato application/hal+json)
    retorno: objeto JSON com uma lista dos endereços cadastrados 

GET: _http://servidor:porta/endereco/{id}_

    descrição: consultar um cadastro existente (seguindo o formato application/hal+json)
    retorno: objeto JSON com o conteúdo do endereço cadastrado

DELETE: _http://servidor:porta/endereco/{id}_

    descrição: excluir um cadastro
    retorno: vazio

// TODO: documentar HTTP response codes para cada método