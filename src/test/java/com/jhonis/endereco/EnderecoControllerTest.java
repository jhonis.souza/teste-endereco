package com.jhonis.endereco;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhonis.endereco.business.EnderecoService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class EnderecoControllerTest {

    private final String msgCamposObrigatorios = "Preencha todos os campos obrigatórios";
    private final String msgCepInexistente = "CEP informado não existe";
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private EnderecoService enderecoService;
    @InjectMocks
    private EnderecoController enderecoController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(enderecoController).build();
    }

    @Test
    public void salvarEnderecoEmBranco() throws Exception {
        when(enderecoService.save(new Endereco())).thenThrow(new Exception("Preencha todos os campos obrigatórios"));
        mockMvc.perform(post("/save")
                .header("Content-Type", "application/json"))
                .andExpect(status().isBadRequest());
        verify(enderecoService, never()).save(new Endereco());
        verifyNoMoreInteractions(enderecoService);
    }

    @Test
    public void salvarComCepInvalido() throws Exception {
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Rua Teste");
        endereco.setNumero("123");
        String cep = "99999999";
        endereco.setCep(cep);
        endereco.setCidade("São Paulo");
        endereco.setEstado("SP");
        String json = objectMapper.writeValueAsString(endereco);

        when(enderecoService.save(any(Endereco.class))).thenAnswer(invocation -> {
            if (invocation.getArgumentAt(0, Endereco.class).getCep().equals(cep)) {
                throw new Exception(msgCepInexistente);
            }
            return endereco;
        });
        mockMvc.perform(post("/save")
                .content(json)
                .header("Content-Type", "application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(msgCepInexistente));
        verify(enderecoService, times(1)).save(any());
        verifyNoMoreInteractions(enderecoService);
    }

    @Test
    public void salvarSemInformarNumero() throws Exception {
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Rua Teste");
        String cep = "123";
        endereco.setCep(cep);
        endereco.setCidade("São Paulo");
        endereco.setEstado("SP");
        String json = objectMapper.writeValueAsString(endereco);

        when(enderecoService.save(any(Endereco.class))).thenAnswer(invocation -> {
            Endereco end = invocation.getArgumentAt(0, Endereco.class);
            if (end.getNumero() == null || end.getNumero().isEmpty()) {
                throw new Exception(msgCamposObrigatorios);
            }
            return endereco;
        });

        mockMvc.perform(post("/save")
                .content(json)
                .header("Content-Type", "application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(Matchers.containsString("Validation failed")));
//                .andExpect(jsonPath("$.message", is(msg)));
        verify(enderecoService, never()).save(any());
        verifyNoMoreInteractions(enderecoService);
    }

    @Test
    public void salvarComSucesso() throws Exception {
        Endereco endereco = new Endereco();
        String cep = "123";
        endereco.setCep(cep);
        endereco.setLogradouro("Rua Teste");
        endereco.setNumero("15");
        endereco.setCidade("São Paulo");
        endereco.setEstado("São Paulo");

        String json = objectMapper.writeValueAsString(endereco);

        when(enderecoService.save(any(Endereco.class))).thenReturn(endereco);
        mockMvc.perform(post("/save")
                .content(json)
                .header("Content-Type", "application/json"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.cep", is(cep)));
        verify(enderecoService, times(1)).save(any(Endereco.class));
        verifyNoMoreInteractions(enderecoService);
    }

    @Test
    public void atualizarComplemento() throws Exception {
        String complemento = "ap 28";
        Endereco endereco = new Endereco();
        endereco.setCep("13847221");
        endereco.setLogradouro("Rua Teste");
        endereco.setNumero("15");
        endereco.setCidade("São Paulo");
        endereco.setEstado("São Paulo");
        endereco.setComplemento(complemento);

        String json = objectMapper.writeValueAsString(endereco);

        when(enderecoService.save(any(Endereco.class))).thenReturn(endereco);
        mockMvc.perform(put("/save")
                .content(json)
                .header("Content-Type", "application/json"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.complemento", is(complemento)));
        verify(enderecoService, times(1)).save(any(Endereco.class));
        verifyNoMoreInteractions(enderecoService);
    }
}
