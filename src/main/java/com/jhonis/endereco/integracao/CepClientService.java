package com.jhonis.endereco.integracao;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CepClientService {

    @Value("${endereco.teste.cep}")
    private String enderecoTesteCep;

    public LogradouroDTO consultarLogradouroPorCep(String cep) throws Exception {
        try {
            HttpResponse<LogradouroDTO> response = Unirest.get(enderecoTesteCep + "/service/buscar-logradouro-por-cep?cep=" + cep)
                    .asObject(LogradouroDTO.class);
            LogradouroDTO logradouro = response.getBody();
            if (!logradouro.getCep().equals(cep)) {
                throw new Exception("CEP informado não existe");
            }
            return logradouro;
        } catch (UnirestException e) {
            throw new Exception("Erro ao buscar CEP");
        }
    }
}
