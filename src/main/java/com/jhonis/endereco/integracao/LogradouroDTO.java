package com.jhonis.endereco.integracao;

public class LogradouroDTO {

    private String cep;
    private String logradouro;

    LogradouroDTO() {
    }

    LogradouroDTO(String cep, String logradouro) {
        this.cep = cep;
        this.logradouro = logradouro;
    }

    //region getters and setters
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
    //endregion
}
