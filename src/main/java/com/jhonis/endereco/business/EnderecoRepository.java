package com.jhonis.endereco.business;

import com.jhonis.endereco.Endereco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "enderecos", itemResourceRel = "endereco", path = "repo")
interface EnderecoRepository extends CrudRepository<Endereco, Long> {

    @Override
    @RestResource(exported = false)
    Endereco save(Endereco endereco);
}
