package com.jhonis.endereco.business;

import com.jhonis.endereco.Endereco;
import com.jhonis.endereco.integracao.CepClientService;
import com.jhonis.endereco.integracao.LogradouroDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService {

    private CepClientService cepClientService;
    private EnderecoRepository enderecoRepository;

    @Autowired
    public EnderecoService(CepClientService cepClientService, EnderecoRepository enderecoRepository) {
        this.cepClientService = cepClientService;
        this.enderecoRepository = enderecoRepository;
    }

    public Endereco save(Endereco endereco) throws Exception {
        if (endereco == null
                || endereco.getCep() == null || endereco.getCep().isEmpty()
                || endereco.getLogradouro() == null || endereco.getLogradouro().isEmpty()
                || endereco.getNumero() == null || endereco.getNumero().isEmpty()
                || endereco.getCidade() == null || endereco.getCidade().isEmpty()
                || endereco.getEstado() == null || endereco.getEstado().isEmpty()) {
            throw new Exception("Preencha todos os campos obrigatórios");
        }
        LogradouroDTO logradouro = cepClientService.consultarLogradouroPorCep(endereco.getCep());
        if (logradouro == null) {
            throw new Exception("CEP informado não existe");
        }
        return enderecoRepository.save(endereco);
    }
}