package com.jhonis.endereco;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Endereco {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Campo Obrigatório")
    @Column(nullable = false)
    private String logradouro;

    @NotNull(message = "Campo Obrigatório")
    @Column(nullable = false, length = 15)
    private String numero;

    private String complemento;
    private String bairro;

    @NotNull(message = "Campo Obrigatório")
    @Column(nullable = false, length = 15)
    private String cep;

    @NotNull(message = "Campo Obrigatório")
    @Column(nullable = false)
    private String cidade;

    @NotNull(message = "Campo Obrigatório")
    @Column(nullable = false)
    private String estado;

    //region getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    //endregion
}
